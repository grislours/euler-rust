# Project Euler - Rust

Solving Euler problems with Rust.

[![Project Euler](https://projecteuler.net/profile/heretik_31.png)](https://projecteuler.net/)

## Try it out

Run current problem:

```
cargo run --release --bin euler
```

## Special thanks

* [The Rust Programming Language](http://www.rust-lang.org/)
* [Project Euler](http://projecteuler.net/)
* [crates.io](https://crates.io/)
