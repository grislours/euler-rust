
use num_bigint::BigInt;
use crate::utils::benchmark::Benchmark;

pub fn solve() -> u32 {
  let _benchmark = Benchmark::new();
  BigInt::from(2).pow(1000).to_string().chars().map(|c| c.to_digit(10).unwrap()).sum::<u32>()
}
