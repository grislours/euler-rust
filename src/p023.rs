
use crate::utils::benchmark::Benchmark;
use crate::utils::prime::{get_primes_up_to_u64, is_abundant};

pub fn solve() -> u64 {
  let _benchmark = Benchmark::new();
  const LIMIT : u64 = 28_123;
  let primes = get_primes_up_to_u64(LIMIT);
  let abundants = (2..LIMIT).filter(|&n| is_abundant(n, &primes)).collect::<Vec<u64>>();
  (1..LIMIT).filter(|&n|
               !abundants.iter()
                         .filter(|&x| x < &n )
                         .any(|&x| abundants.binary_search_by(|z| z.cmp(&(n - x))).is_ok()))
            .sum()
}
