
extern crate num_traits;

fn reverse<Type_>(n:Type_, base:Type_) -> Type_ where
    Type_ : std::cmp::PartialOrd + Copy + num_traits::Num
{
    let mut result = Type_::zero();
    let mut number = n;
    while number > Type_::zero() {
        result = result * base + number % base;
        number = number / base;
    }
    result
}

pub fn is_palindromic<Type_>(n: Type_, base:Type_) -> bool where
    Type_ : std::cmp::PartialOrd + Copy + num_traits::Num
{
    n == reverse(n, base)
}
