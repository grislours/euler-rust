
use std::time::Instant;

pub struct Benchmark {
  timestamp : Instant,
}

impl Benchmark {
  pub fn new() -> Benchmark {
    Benchmark{ timestamp : Instant::now() }
  }
}

impl Drop for Benchmark {
    fn drop(&mut self) {
        println!("{} µs", self.timestamp.elapsed().as_micros());
    }
}
