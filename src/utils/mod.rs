
#![allow(dead_code)]

pub mod palindrome;
pub mod prime;
pub mod benchmark;
pub mod combinatorics;
