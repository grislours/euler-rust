
pub fn get_primes_up_to_u16(n: u16) -> Vec<u64> {
    let mut primes: Vec<u64> = Vec::new();
    if n < 2 {
        return primes;
    }
    let is_primes_size = n as usize + 1;
    let mut is_primes = vec![true; is_primes_size];
    for i in 2..is_primes_size {
        if is_primes[i] {
            primes.push(i as u64);
            for j in (i * i..is_primes_size).step_by(i) {
                is_primes[j] = false;
            }
        }
    }
    primes
}

fn push_next_primes(primes: &mut Vec<u64>, lower: u64, upper: u64) {
    if lower > upper {
        panic!("'upper' is lower than 'lower'...");
    }
    let max_prime = (upper as f32).sqrt() as u64;
    let mut is_prime_values = vec![true; (upper - lower + 1) as usize];
    let mut i = 0;
    loop {
        let prime = primes[i];
        if prime > max_prime {
            break;
        }
        let remainder = lower % prime;
        if remainder == 0 {
            is_prime_values[0] = false;
        }
        for j in (prime - remainder..is_prime_values.len() as u64).step_by(prime as usize) {
            is_prime_values[j as usize] = false;
        }
        i += 1;
    }

    // push primes
    for (i, is_prime) in is_prime_values.iter().enumerate() {
        if *is_prime {
            primes.push(lower + i as u64);
        }
    }
}

pub fn get_primes_up_to_u64(n: u64) -> Vec<u64> {
    // for low values
    if n < u16::max_value() as u64 {
        return get_primes_up_to_u16(n as u16);
    }

    // for other values
    let delta = 200_000u64;
    let mut primes = get_primes_up_to_u16(u16::max_value());
    for lower in (u16::max_value() as u64 + 1..n).step_by(delta as usize) {
        let upper = std::cmp::min(n, lower + delta);
        push_next_primes(&mut primes, lower, upper);
    }
    primes
}

pub fn factorize(x: u64) -> Vec<(u64, u32)> {
    let primes = get_primes_up_to_u64((x as f64).sqrt().ceil() as u64);
    factorize_with_primes(x, &primes)
}

pub fn factorize_with_primes(x: u64, primes : &Vec<u64>) -> Vec<(u64, u32)> {
    let mut result : Vec<(u64, u32)> = Vec::new();

    // start from the end
    let mut n = x;
    let mut index = primes.len() - 1;
    while n != 1 {
        let prime = primes[index];
        let mut power = 0;
        while n % prime == 0 {
            n /= prime;
            power += 1;
        }
        if power > 0 {
            result.push((prime, power));
            let sqrt = (n as f64).sqrt().floor() as u64;
            index = primes.binary_search(&sqrt).unwrap_or_else(|x| x);
        } else {
            if index == 0 {
                result.push((n, 1));
                break;
            }
            index -= 1;
        }
    }
    result.sort_unstable_by(|l, r| l.0.cmp(&r.0));
    result
}

pub fn sigma0(n :u64) -> u32 {
  factorize(n).iter().map(|(_, a)| a + 1).product::<u32>()
}

pub fn sigma1(n :u64) -> u64 {
  factorize(n).iter().map(|(p, a)| (p.pow(a + 1) - 1) / (p - 1)).product::<u64>()
}

pub fn sigma1_with_primes(n :u64, primes : &Vec<u64>) -> u64 {
  factorize_with_primes(n, primes).iter().map(|(p, a)| (p.pow(a + 1) - 1) / (p - 1)).product::<u64>()
}

pub fn aliquot_sum(n :u64) -> u64 {
  sigma1(n) - n
}

pub fn aliquot_sum_with_primes(n :u64, primes :&Vec<u64>) -> u64 {
  sigma1_with_primes(n, primes) - n
}

pub fn is_amicable(n :u64, primes : &Vec<u64>) -> bool {
  let a = aliquot_sum_with_primes(n, &primes);
  let b = aliquot_sum_with_primes(a, &primes);
  a != 1 && a != n && b == n
}

pub fn is_abundant(n :u64, primes : &Vec<u64>) -> bool {
  aliquot_sum_with_primes(n, &primes) > n
}

pub fn phi(p_and_ks :Vec<(u64, u32)>) -> u64 {
  p_and_ks.iter().fold(1, |acc, &(p, k)| acc * p.pow(k - 1) * (p - 1))
}
