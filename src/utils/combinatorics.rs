
extern crate num_rational;
use num_rational::Ratio;

pub fn small_factorial(n :u8) -> u64 {
  const RESULTS : [u64; 21] = [
    1, 1, 2, 6, 24, 120, 720, 5040, 40320, 362880, 3628800, 39916800, 479001600,
    6227020800, 87178291200, 1307674368000, 20922789888000, 355687428096000,
    6402373705728000, 121645100408832000, 2432902008176640000
  ];
  RESULTS[n as usize]
}

pub fn combination(n :u32, k:u32) -> Ratio<u64> {
  Ratio::new(small_factorial(n as u8), small_factorial(k as u8) * small_factorial((n - k) as u8))
}

pub fn nth_permutation(v: &[u8], mut nth: usize) -> Option<Vec<u8>> {
  let mut r = Vec::new();
  for i in 0..v.len() {
    let f = small_factorial((v.len() - i - 1) as u8) as usize;
    for j in 0..(v.len() as u8) {
      if r.contains(&j) {
        continue;
      }
      if nth < f {
        r.push(j);
        break;
      }
      nth -= f;
    }
  }
  if v.len() == r.len() {
    Some(r)
  } else {
    None
  }
}
