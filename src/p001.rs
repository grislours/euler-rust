
fn sum_multiples(n:i32) -> i32 {
  (0..n).filter(|i| i % 3 == 0 || i % 5 == 0).sum()
}

pub fn solve() -> i32 {
   sum_multiples(1000)
}
