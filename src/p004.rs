
use crate::utils::palindrome::is_palindromic;

pub fn solve() -> u32 {
    let mut products : Vec<u32> = Vec::new();
    for n1 in (1..=999).rev() {
        for n2 in (n1..=999).rev() {
            let n = n1 * n2;
            if is_palindromic(n, 10) {
                let index = products.binary_search(&n).unwrap_or_else(|i| i);
                products.insert(index, n);
            }
        }
    }
    *products.last().unwrap()
}
