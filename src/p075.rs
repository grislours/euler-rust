
use crate::utils::benchmark::Benchmark;
use crate::num_integer::{Integer, gcd};

pub fn solve() -> u32 {
  let _benchmark = Benchmark::new();

  const LIMIT : usize = 1_500_000;
  let mut counts = [0; LIMIT + 1];

  let mut m = 2;
  let mut c = 0;
  while c < LIMIT {

    for n in 1..m {
      let a = m * m - n * n;
      let b = 2 * m * n;
      c = m * m + n * n;

      if c > LIMIT {
        break;
      }

      if gcd(m, n) != 1 || (m.is_odd() && n.is_odd()) {
        continue;
      }

      let l = a + b + c;
      (1..).map(|x| l * x).take_while(|&l| l <= LIMIT).for_each(|l| counts[l] += 1);
    }

    m += 1;
  }

  counts.iter().filter(|&x| *x == 1).sum()
}
