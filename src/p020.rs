
use num_bigint::BigInt;
use crate::utils::benchmark::Benchmark;

pub fn solve() -> u32 {
  let _benchmark = Benchmark::new();
  let mut n = BigInt::from(1);
  (1..=100).for_each(|x| n *= x);
  n.to_string().chars().map(|c| c.to_digit(10).unwrap()).sum::<u32>()
}
