
use crate::utils::prime::factorize;
use std::collections::HashMap;

pub fn solve() -> u64 {
    let mut factor_and_powers = HashMap::new();
    for n in 2..=20 {
        let f = factorize(n);
        for (p, pow) in &f {
            if let Some(power) = factor_and_powers.get_mut(p) {
                *power = std::cmp::max(*power, *pow);
            } else {
                factor_and_powers.insert(*p, *pow);
            }
        }
    }

    // expansion
    let mut result = 1;
    for (p, pow) in &factor_and_powers {
        result *= p.pow(*pow);
    }

    // return
    result
}
