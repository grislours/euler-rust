
use crate::utils::benchmark::Benchmark;
use crate::utils::combinatorics::nth_permutation;

pub fn solve() -> Vec<u8> {
  let _benchmark = Benchmark::new();
  let v = b"0123456789";
  let nth = 1_000_000;
  nth_permutation(v, nth - 1).unwrap()
}
