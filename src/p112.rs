
#[derive(PartialEq)]
enum Property {
  UNKNOWN,
  INCREASING,
  DECREASING,
  BOUNCY,
}

fn is_bouncy(n: u32) -> bool {
  let mut result = Property::UNKNOWN;
  let mut last_c_opt : Option<char> = None;
  for c in n.to_string().chars() {
    if let Some(last_c) = last_c_opt {
      if result == Property::UNKNOWN && c != last_c {
        result = if last_c < c { Property::INCREASING } else { Property::DECREASING };
      } else if result == Property::INCREASING && c < last_c {
        return true;
      } else if result == Property::DECREASING && c > last_c {
        return true;
      }
    }
    last_c_opt = Some(c);
  }
  result == Property::BOUNCY
}

pub fn solve() -> u32 {
  let mut sum = 0;
  for i in 1.. {
    sum += if is_bouncy(i) { 1 } else { 0 };
//    println!("{} {}", i, 100 * (i-sum)/i);
    if 100 * (i - sum) == i {
      return i;
    }
  }
  0
}
