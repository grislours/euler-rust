
fn compute(n: u64) -> u64 {
    let sum : u64 = (1..=n).sum();
    let square_of_the_sum = sum * sum;
    let mut sum_of_squares = 0;
    for i in 1..=n {
        sum_of_squares += i * i;
    }
    square_of_the_sum - sum_of_squares
}

pub fn solve() -> u64 {
    compute(100)
}
