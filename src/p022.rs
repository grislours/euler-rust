
use crate::utils::benchmark::Benchmark;
use std::fs;

pub fn solve() -> u32 {
  let _benchmark = Benchmark::new();
  let file = fs::read_to_string("./assets/p022_names.txt").unwrap();
  let mut names = file
      .trim()
      .split(',')
      .collect::<Vec<&str>>();
  names.sort();
  names.iter()
        .zip(1..)
        .map(|(&str, i)| {
            str[1..str.len() - 1].chars()
            .map(|c| c as u32 - 'A' as u32 + 1)
            .sum::<u32>() * i
        })
        .sum()
}
