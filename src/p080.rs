
use crate::utils::benchmark::Benchmark;
use num_bigint::BigUint;

pub fn solve() -> u64 {
  let _benchmark = Benchmark::new();

  const PRECISION : u32 = 1_000;
  (1u64..=100)
    .filter(|&n| {let sqrt = (n as f32).sqrt() as u64; sqrt * sqrt != n })
    .map(|n| {
      let sqrt = (BigUint::from(n) * BigUint::from(10u64).pow((PRECISION - 1) * 2)).sqrt();
      sqrt.to_string().chars().map(|c| c.to_digit(10).unwrap() as u64).sum::<u64>()
  }).sum()
}
