
use crate::utils::benchmark::Benchmark;
use crate::utils::prime::{factorize_with_primes, phi, get_primes_up_to_u64};

pub fn solve() -> u64 {
  let _benchmark = Benchmark::new();
  const LIMIT : u64 = 1_000_000;
  let primes = get_primes_up_to_u64((LIMIT as f64).sqrt() as u64);
  (2..=LIMIT).fold(0, |acc, i| phi(factorize_with_primes(i, &primes)) + acc)
}
