
pub fn solve() -> u64 {
  let w = 20 + 1;
  let h = 20 + 1;
  let mut array = vec![vec![0; w]; h];
  array[0][0] = 1;
  for l in 0..h {
    for c in 0..w {
      array[l][c] = if l == 0 && c == 0 { 1 } else {
        (if c != 0 { array[l][c - 1] } else { 0 }) + if l != 0 { array[l - 1][c] } else { 0 }
      }
    }
  }
  array[h - 1][w - 1]
}
