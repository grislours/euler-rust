
use num_traits::FromPrimitive;

#[derive(FromPrimitive, PartialEq)]
pub enum WeekDay {
  Monday    = 0,
  Tuesday   = 1,
  Wednesday = 2,
  Thursday  = 3,
  Friday    = 4,
  Saturday  = 5,
  Sunday    = 6,
}

fn get_days_of_each_month(is_leap_year : bool) -> [u32;12] {
  if is_leap_year {
    [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
  } else {
    [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
  }
}

fn is_leap_year(year: u32) -> bool {
  if year % 4 == 0 {
    if year % 100 != 0 {
      true
    } else {
      if year % 400 == 0 {
        true
      } else {
        false
      }
    }
  } else {
    false
  }
}

fn next_weekday(weekday : WeekDay, days : u32) -> WeekDay {
  WeekDay::from_u32(((weekday as u32) + days) % 7).unwrap()
}

fn get_sundays_and_next_weekday(weekday : WeekDay, is_leap_year : bool) -> (u32, WeekDay) {
  let days_of_each_month = get_days_of_each_month(is_leap_year);

  // compoute sundays
  let mut sundays = 0;
  let mut month_weekday = weekday;
  for days_count in days_of_each_month.iter() {
    sundays += if month_weekday == WeekDay::Sunday { 1 } else { 0 };
    month_weekday = next_weekday(month_weekday, *days_count);
  }

  // return result
  (sundays, month_weekday)
}

pub fn solve() -> u32 {
  let mut weekday = WeekDay::Tuesday;
  let mut sundays = 0;
  for year in 1901..=2000 {
    let sundays_and_next_weekday = get_sundays_and_next_weekday(weekday, is_leap_year(year));
    sundays += sundays_and_next_weekday.0;
    weekday = sundays_and_next_weekday.1;
  }
  sundays
}
