
use crate::utils::benchmark::Benchmark;
use std::fs;
use std::cmp;
use std::vec::Vec;

pub fn solve() -> u32 {
  let _benchmark = Benchmark::new();
  let file = fs::read_to_string("./assets/p067_triangle.txt").unwrap();
  let mut grid : Vec<Vec<u32>> = file
      .trim()
      .lines()
      .map(|line| {
          line.split_whitespace()
              .map(|x| { x.parse().unwrap() })
              .collect()
          })
      .rev()
      .collect();

  let h = grid.len();
  let w = grid[0].len();

  for l in 0..h - 1 {
    for c in 0..w - l - 1 {
      let max = cmp::max(grid[l][c], grid[l][c + 1]);
      grid[l + 1][c] += max;
    }
  }

  grid[h - 1][0]
}
