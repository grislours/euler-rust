
use std::collections::HashMap;

fn collatz_n(cache :&mut HashMap<u64, u32>, n :u64) -> u32 {
  match cache.get(&n) {
    Some(result) => *result,
    None => if n % 2 == 0 {
        collatz_n(cache, n / 2) + 1
      } else {
        collatz_n(cache, 3 * n + 1) + 1
      }
  }
}

pub fn solve() -> u64 {
  // construct cache
  let mut cache = HashMap::new();
  cache.insert(1, 1);

  // construct max
  let mut max_i = 0;
  let mut max = 0;

  for i in 1..1_000_000 {
    let n = collatz_n(&mut cache, i);
    if n > max {
      max = n;
      max_i = i;
    }
  }
  max_i
}
