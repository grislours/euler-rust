
use crate::utils::prime::get_primes_up_to_u64;

pub fn solve() -> u64 {
    get_primes_up_to_u64(110_000)[10_001]
}
