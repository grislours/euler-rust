
use crate::utils::benchmark::Benchmark;
use num_rational::Ratio;
use std::vec::Vec;

fn sum_probabilities(dice_count : u32, face_count : u32) -> Vec<Ratio<u64>> {
  let mut result : Vec<u64> = Vec::with_capacity(face_count as usize);
  result.resize(face_count as usize, 1);
  for i in 2..=dice_count {
    let new_size = result.len() + face_count as usize;
    let mut new = Vec::with_capacity(new_size);
    new.resize(new_size, 0);
    for j in i..=new_size as u32 {
      let mut sum = 0;
      for k in 1..=face_count {
        if j >= k + 1 && j < result.len() as u32 + k + 1 {
          sum += result[(j - k - 1) as usize];
        }
      }
      new[(j - 1) as usize] = sum;
    }
    result = new;
  }

  // add ratio
  let total = (face_count as u64).pow(dice_count);
  result.iter().map(|&x| Ratio::new(x, total)).collect()
}

pub fn solve() -> u64 {
  let _benchmark = Benchmark::new();

  const COLIN_DICE_COUNT : u32 = 6;
  const COLIN_FACE_COUNT : u32 = 6;
  let colin_sum_probabilities = sum_probabilities(COLIN_DICE_COUNT, COLIN_FACE_COUNT);
  const PETER_DICE_COUNT : u32 = 9;
  const PETER_FACE_COUNT : u32 = 4;
  let peter_sum_probabilities = sum_probabilities(PETER_DICE_COUNT, PETER_FACE_COUNT);

  // compute probability of colin doing more than 34
  let result : Ratio<u64> = colin_sum_probabilities
               .iter()
               .enumerate()
               .map(|(total, &p1)| p1 * peter_sum_probabilities.iter().skip(total + 1).sum::<Ratio<u64>>())
               .sum();
  10_u64.pow(8) * result.numer() / result.denom()
}
