
use crate::utils::prime::{is_amicable, get_primes_up_to_u16};
use crate::utils::benchmark::Benchmark;

pub fn solve() -> u64 {
  let _benchmark = Benchmark::new();
  const LIMIT : u64 = 10_000;
  let primes = get_primes_up_to_u16((LIMIT as f32).sqrt().ceil() as u16);
  (2u64..=LIMIT).filter(|&n| is_amicable(n, &primes)).sum()
}
