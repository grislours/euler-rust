
fn sum_even_fibonacci(max:u32) -> u32 {
  let mut n2 = 1u32;
  let mut n1 = 2u32;
  let mut sum = 2u32;
  loop {
    let n = n1 + n2;
    if n > max {
      break;
    }
    n2 = n1;
    n1 = n;
    if n % 2 == 0 {
      sum += n;
    }
  }
  sum
}

pub fn solve() -> u32 {
  sum_even_fibonacci(4_000_000)
}
