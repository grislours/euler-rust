
use crate::utils::factorize;

pub fn solve() -> u64 {
    let factors = factorize(600_851_475_143);
    factors.last().unwrap().0
}
