
use lazy_static::lazy_static;
use std::collections::HashMap;
use crate::utils::benchmark::Benchmark;

lazy_static!{
    static ref NUMBER_AND_STRINGS: HashMap<u32, &'static str> = vec![
    (    1, "one"       ),
    (    2, "two"       ),
    (    3, "three"     ),
    (    4, "four"      ),
    (    5, "five"      ),
    (    6, "six"       ),
    (    7, "seven"     ),
    (    8, "eight"     ),
    (    9, "nine"      ),
    (   10, "ten"       ),
    (   11, "eleven"    ),
    (   12, "twelve"    ),
    (   13, "thirteen"  ),
    (   14, "fourteen"  ),
    (   15, "fifteen"   ),
    (   16, "sixteen"   ),
    (   17, "seventeen" ),
    (   18, "eighteen"  ),
    (   19, "nineteen"  ),
    (   20, "twenty"    ),
    (   30, "thirty"    ),
    (   40, "forty"     ),
    (   50, "fifty"     ),
    (   60, "sixty"     ),
    (   70, "seventy"   ),
    (   80, "eighty"    ),
    (   90, "ninety"    ),
    (  100, "hundred"   ),
    ( 1000, "thousand"  ),
    ].into_iter().collect();
}

fn letters(number:u32) -> u32 {
  if number == 0 {
    0
  } else if number <= 20 {
    NUMBER_AND_STRINGS.get(&number).unwrap().len() as u32
  } else if number < 100 {
    NUMBER_AND_STRINGS.get(&(number / 10 * 10)).unwrap().len() as u32 + letters(number % 10)
  } else if number < 1000 && number % 100 == 0 {
    letters(number / 100) + NUMBER_AND_STRINGS.get(&100).unwrap().len() as u32
  } else if number < 1000 {
    letters(number % 100) +
    NUMBER_AND_STRINGS.get(&(number / 100)).unwrap().len() as u32 +
    NUMBER_AND_STRINGS.get(&100).unwrap().len() as u32 + 3
  } else {
    letters(1) + NUMBER_AND_STRINGS.get(&1000).unwrap().len() as u32
  }
}

pub fn solve() -> u32 {
  let benchmark = Benchmark::new();
  (1..=1000).map(|n| letters(n)).sum::<u32>()
}
