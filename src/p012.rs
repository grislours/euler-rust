
use crate::utils::prime::sigma0;

fn triangle() -> u64 {
  let mut triangle = 0;
  for i in 1.. {
    triangle += i;
    if i > 1 && sigma0(triangle) > 500 {
      return triangle;
    }
  };
  0
}

pub fn solve() -> u64 {
  triangle()
}
